'use strict';

const connectToDatabase = require('./connect.js');
const playerModels = require('./dbModels.js');
const schemas = require('./schemas.js');

const handler = (request, reply) => {
  const model = playerModels[request.params.region];

  model.find({ summoner: request.params.summoner }).then(res => {
    if (!res.length) {
      const player = new model({
        summoner: request.params.summoner,
        summonerId: '123'
      });

      player.save();
    }
  }).catch(err => {
    console.log(err);
  });
};

module.exports.register = (server, options, next) => {
  server.route({
    method: 'GET',
    path: '/{region}/summoners/{summoner}',
    handler,
    config: {
      validate: schemas
    }
  });

  connectToDatabase();
  next();
}

module.exports.register.attributes = {
  name: 'storage',
  version: '1.0.0'
};
