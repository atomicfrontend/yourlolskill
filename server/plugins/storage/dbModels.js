'use strict';

const regions = require('./../../utils/regions.js');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const defaultSchema = new Schema(
  {
    summoner: { type: String, lowercase: true, index: true },
    summonerId: Number,
    updated: { type: Date, default: Date.now }
  }
);

const schemaMap = {};

regions.forEach(region => {
  schemaMap[region] = mongoose.model(region + 'player', defaultSchema);
});

module.exports = schemaMap;
