'use strict';

const mongoose = require('mongoose');
const r = require('ramda');

module.exports = r.once(() => {
  const options = {
    user: 'admin',
    pass: 'Password'
  };

  mongoose.connect('mongodb://localhost/lolskill');
  const db = mongoose.connection;

  db.on('error', () => {
    console.log('Failed to connect to database');
  });

  db.once('open', () => {
    console.log('Connected to database');
  });
});
