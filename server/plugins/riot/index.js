'use strict';

const Wreck = require('wreck');
const get = require('../utils/requests').get;

const getPlayerId = name => {
  const path = `https://euw.api.pvp.net/api/lol/euw/v1.4/summoner/by-name/${name}?`;

  return get(path)
         .then(res => Promise.resolve(JSON.parse(res)[name].id))
         .catch(err => Promise.reject(err));
};

const getGames = summonerId => {
  const path = `https://euw.api.pvp.net/api/lol/euw/v2.2/matchlist/by-summoner/${summonerId}?rankedQueues=TEAM_BUILDER_DRAFT_RANKED_5x5&`

  return get(path)
        .then(res => Promise.resolve(JSON.parse(res)))
        .catch(err => Promise.reject(err));
};

const getGameDetails = game => {
  const gameId = games.matches[0].matchId;
  const path = `https://euw.api.pvp.net/api/lol/euw/v2.2/match/${gameId}?`;

  return get(path)
         .then(res => Promise.resolve(JSON.parse(res)))
         .catch(err => Promise.reject(err));
};

const handler = (request, reply) => {
  getPlayerId(request.params.playerName)
    .then(res => getGames(res))
    .then(res => getGameDetails(res))
    .then(res => reply(res))
    .catch(err => reply(err));
};

module.exports.register = (server, options, next) => {
  server.route({
    method: 'GET',
    path: '/games/{playerName}',
    handler
  });

  next();
}

module.exports.register.attributes = {
  name: 'fetchPlayerData',
  version: '1.0.0'
};
