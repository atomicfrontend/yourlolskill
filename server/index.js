'use strict';

const Hapi = require('hapi');
const inert = require('inert');
const Wreck = require('wreck');
const storage = require('./plugins/storage');

// Server settings
const options = {
  connections: {
    router: {
      isCaseSensitive: false
    }
  }
};

module.exports = () => {
  const server = new Hapi.Server(options);
  server.connection({ port: 3000 });

  server.start(err => {
    if (err) {
      throw err;
    }

    console.log('Server running');
  });

  server.register(inert, err => {
    if (err) {
      throw err;
    }

    server.route({
      method: 'GET',
      path: '/',
      handler: (request, reply) => {
        reply.file('./static/index.html');
      }
    });
  });

  server.register(storage, err => {
    if (err) throw err;
  });
};
