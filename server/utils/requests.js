'use strict';

const Wreck = require('wreck');
const Boom = require('boom');
const apiKey = '9271c82f-763d-46ed-8716-2574024e1daa';

module.exports.get = path => {
  const url = path + `api_key=${apiKey}`;

  return new Promise((resolve, reject) => {
    Wreck.get(url, (err, res, payload) => {
      if (err) return reject(err);

      if (res.statusCode !== 200) {
        return reject(Boom.create(res.statusCode, res.statusMessage));
      }

      return resolve(payload.toString());
    });
  });
};
